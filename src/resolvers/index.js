const { Query } = require('./Query')
const AuthPayload = require('./AuthPayload')
const auth = require('./Mutation/auth')
const setup = require('./Mutation/setup')
const repo = require('./Mutation/repo')

module.exports = {
  Query: {
    ...Query
  },
  Mutation: {
    ...auth,
    ...setup,
    ...repo
  },
  AuthPayload,
}
