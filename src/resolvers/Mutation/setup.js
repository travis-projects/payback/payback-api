const { getUserId } = require('../../utils')

const setup = {
  setup (parent, { amount, currency }, ctx, info) {
    const userId = getUserId(ctx)
    return ctx.db.mutation.updateUser({
      where: { id: userId },
      data: { amount, currency, setup: true }
    })
  }
}

module.exports = setup
