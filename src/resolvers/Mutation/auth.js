const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { getUserId } = require('../../utils')
const axios = require('axios')
const queryString = require('query-string')

const auth = {
  async signup(parent, args, ctx, info) {
    const password = await bcrypt.hash(args.password, 10)
    const user = await ctx.db.mutation.createUser({
      data: { ...args, password },
    })

    return {
      token: jwt.sign({ userId: user.id }, process.env.APP_SECRET),
      user,
    }
  },

  async login(parent, { email, password }, ctx, info) {
    const user = await ctx.db.query.user({ where: { email } })
    if (!user) {
      throw new Error(`No such user found for email: ${email}`)
    }

    const valid = await bcrypt.compare(password, user.password)
    if (!valid) {
      throw new Error('Invalid password')
    }

    return {
      token: jwt.sign({ userId: user.id }, process.env.APP_SECRET),
      user,
    }
  },
  storeState(parent, { state }, ctx, info) {
    const userId = getUserId(ctx)
    return ctx.db.mutation.updateUser({
      where: { id: userId },
      data: { storedState: state }
    })
  },
  async authorise(parent, { code, state }, ctx, info) {
    const userId = getUserId(ctx)
    try {
      try {
        const { storedState } = await ctx.db.query.user({ where: { id: userId } })
        if (state !== storedState) throw new Error('State does match')
      } catch (err) {
        throw new Error(err.message)
      }
      const { data } = await axios({
        method: 'post',
        url: 'https://github.com/login/oauth/access_token',
        params: {
          'client_id': process.env.GITHUB_CLIENT_ID,
          'client_secret': process.env.GITHUB_CLIENT_SECRET,
          code,
          state
        }
      })
      const { access_token } = await queryString.parse(data)
      // Add token to user
      return ctx.db.mutation.updateUser({
        where: { id: userId },
        data: { token: access_token }
      })
    } catch (err) {
      throw new Error(err.message)
    }
  }
}

module.exports = auth
