const { getUserId } = require('../../utils')

const repo = {
  async addRepo (parent, { repo, dependencies }, ctx, info) {
    // Auth User
    const userId = getUserId(ctx)
    // function  to check if exists
    function exists(name) {
      return ctx.db.query.dependency({ where: { name } }).then((result) => {
        if (!result) return createDep(name)
        else return linkDep(result.id)
      }).then(result => ({ id: result.id })).catch(err => console.log(err))
    }
    // Create dep
    function createDep(name) {
      return ctx.db.mutation.createDependency({ data: { name, contributors: { connect: { id: userId }} }}).then((response) => response).catch(err => console.log(err))
    }
    // Link Dependancy and contributor
    function linkDep(id) {
      return ctx.db.mutation.updateDependency({ where: { id }, data: { contributors: { connect: { id: userId }} } })
    }
    // Loop through checks
    async function check(dependencies) {
      const arrayOfPromises = dependencies.map(dep => exists(dep))
      return await Promise.all(arrayOfPromises)
    }
    // Run Checks
    const connectDeps = await check(dependencies)
    // Create Repo
    return ctx.db.mutation.createRepo({
      data: {
        ...repo,
        dependencies: {
          connect: connectDeps
        },
        user: {
          connect: { id: userId }
        }
      }
    })
  }
}

module.exports = repo
