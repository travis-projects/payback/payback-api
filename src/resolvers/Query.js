const { getUserId } = require('../utils')

const Query = {
  user (parent, args, ctx, info) {
    const id = getUserId(ctx)
    return ctx.db.query.user({ where: { id } }, info)
  },
  repos (parent, args, ctx, info) {
    const id = getUserId(ctx)
    return ctx.db.query.repoes({ where: { user: { id } } }, info)
  }
}

module.exports = { Query }
